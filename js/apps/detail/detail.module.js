define([
    // Defaults
    "jquery",
    "angular",
    "angularSanitize"
], function($){
    "use strict";

    var App = {

        init: function(MainApp) {
            // init App
            // ------------------------------------------------------------------------

            MainApp.keepoApp.factory("comments", function ($http, $q, accessToken) {
                return {
                    get: function (slug) {
                        var feed_url = MainApp.baseURL + 'api/v1/comment?slug=' + slug;
                        // get the token to access api
                        return accessToken.get().then(
                            function (token) {
                                // token received, now get the feeds data
                                return $http.get(feed_url, token).then(
                                    function (response) {
                                        return response.data;
                                    },
                                    function (response) {
                                        return $q.reject(response.data);
                                    }
                                );
                            }
                        );

                    }
                }
            });

            MainApp.keepoApp.directive('body', function ($document, $window, comments) {
                return {
                    restrict: "E",
                    link: function (scope, element, attrs) {

                        scope.loadComments = function (slug) {
                            // get slug
                            var theSlug = slug;
                            if (slug == undefined) {
                                console.log($window.location.href);
                                var url = $window.location.href;
                                var tempSplit = url.split("/");
                                var dirtySlug = tempSplit[tempSplit.length - 1];
                                // removing query
                                tempSplit = dirtySlug.split("?");
                                dirtySlug = tempSplit[0];
                                // removing hash
                                tempSplit = dirtySlug.split("#");
                                theSlug = tempSplit[0];
                                console.log(theSlug);
                            }
                            // load comment from API start here
                            comments.get(theSlug).then(function (data) {
                                scope.comments = data;
                            }, function (data) {
                                console.log("error when loading new feeds");
                            })
                        }

                    }
                };
            });


            MainApp.keepoApp.directive("comment", function () {
                return {
                    restrict: "E",
                    replace: true,
                    templateUrl: "commentTemplate",
                    controller: ['$scope', 'comments', function ($scope, comments) {
                        $scope.comments = [];
                    }]
                }
            });

            // start up the module
            MainApp.keepoApp.run();
            angular.bootstrap(document.querySelector("html"), ["keepoApp"]);
        }
    };

    return App;
});